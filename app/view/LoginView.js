Ext.define('TestOne.view.LoginView', {
    extend: 'Ext.Panel',
    xtype: 'login',
    requires: [
        'Ext.TitleBar',
    ],
    config: {
        items: [
            {
                title: 'Login',
                xtype: 'titlebar',
                items: [
                    {
                        xtype: 'button',
                        action: 'signup',
                        text: 'Sign up',
                        align: 'right'
                    }
                ]
            },
            {

            }
        ]
    }
});
