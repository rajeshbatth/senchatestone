Ext.define('TestOne.view.SignUpView', {
    extend: 'Ext.form.Panel',
    xtype: 'signup',
    requires: [
        'Ext.TitleBar',
        'Ext.form.FieldSet',
        'Ext.form.Email',
        'Ext.form.Password'
    ],
    config: {
        items: [
            {
                title: 'Sign up',
                xtype: 'titlebar',
                items: [
                    {
                        xtype: 'button',
                        action: 'back',
                        text: 'back',
                        ui: 'back',
                        align: 'left'
                    }
                ]
            },
            {
                xtype: 'fieldset',
                items: [
                    {
                        xtype: 'textfield',
                        name: 'name',
                        placeHolder: 'Name'
                    },
                    {
                        xtype: 'emailfield',
                        name: 'email',
                        placeHolder: 'Email'
                    },
                    {
                        xtype: 'passwordfield',
                        name: 'password',
                        placeHolder: 'Password'
                    },
                    {
                        xtype: 'button',
                        name: 'submit',
                        text: 'Submit',
                        action:'signup_submit'
                    }
                ]
            }
        ]
    }
});
