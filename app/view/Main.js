Ext.define('TestOne.view.Main', {
    extend: 'Ext.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar',
    ],
    config: {
        items: [
            {
                title: 'Login',
                xtype: 'titlebar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Sign up',
                        align: 'right'
                    }
                ]
            },
            {

            }
        ]
    }
});
