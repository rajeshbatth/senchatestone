Ext.define('TestOne.controller.LoginController', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
            loginView: 'login',
            signUpView: 'signup'
        },
        control: {
            "login button[action=signup]": {
                tap: 'onSignUpClicked'
            },
            "button[action=back]": {
                tap: 'onBack'
            },
            "button[action=signup_submit]": {
                tap: 'onSignUpSubmitClicked'
            }
        },
        routes: {
            '': 'showLoginPage', //Default page
            'user/signup': 'onSignUpClicked'
        },
        history: null
    },

    init: function () {
        this.setHistory(this.getApplication().getHistory());
    },

    addToHistory: function (id) {
        this.getHistory().add(new Ext.app.Action({
            url: id
        }), true);
    },

    onBack: function () {
        history.back();
        return false;
    },

    showLoginPage: function () {
        Ext.Viewport.setActiveItem('login')
    },

    onSignUpClicked: function () {
        if (Ext.Viewport.down('signup') == null) {
            Ext.Viewport.add({xtype: 'signup'});
        }
        //Ext.Viewport.animateActiveItem('signup', {type: 'slide', direction: 'left'});
        Ext.Viewport.setActiveItem('signup');
        this.addToHistory("user/signup")
    },

    onSignUpSubmitClicked:function(){
        var values = this.getSignUpView().getValues();
        var newUser = Ext.create('TestOne.model.User', values);
        var userStore = Ext.StoreManager.get('UserStore');
        userStore.add(newUser);
        userStore.sync();
    }


});