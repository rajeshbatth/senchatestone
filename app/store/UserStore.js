Ext.define('TestOne.store.UserStore', {
    extend: 'Ext.data.Store',
    config: {
        model: 'TestOne.model.User',
        autoLoad: true,

        proxy: {
            type: 'localstorage',
            id: 'userstore'
        }
    }
});