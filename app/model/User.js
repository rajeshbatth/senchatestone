Ext.define('TestOne.model.User', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            { name: 'name', type: 'string' },
            { name: 'email', type: 'string' },
            { name: 'password', type: 'string' }
        ]
    }
});